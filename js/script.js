//підтягую елементи
let body = document.querySelector("body");
let footer = document.querySelector("footer");
let main = document.getElementById("clients__projects__wrap");
let pCollection = document.querySelectorAll("p");
let aCollection = document.querySelectorAll("a");
let h1 = document.querySelector("h1");
let h2 = document.querySelector("h2");
let h3 = document.querySelector("h3");

//створюю кнопку
let support = document.getElementById("support");
let btn = document.createElement("button");
btn.classList.add("button_size");
support.appendChild(btn);
btn.textContent = "Змінити тему";
btn.addEventListener("click", changeTheme);

//функціонал кнопки
let themeNummber = 0;

function changeTheme() {
  if (themeNummber === 1) {
    removeTheme();
    themeNummber -= 1;
    localStorage.setItem("myVariable", themeNummber);
    return;
  }

  if (themeNummber === 0) {
    applyTheme();
    themeNummber += 1;
    localStorage.setItem("myVariable", themeNummber);
  }
}

function applyTheme() {
  body.classList.add("change_theme_background1");
  footer.classList.add("change_theme_background1");
  main.classList.add("change_theme_background2");
  h1.classList.add("change_theme_textColor");
  h2.classList.add("change_theme_textColor");
  h3.classList.add("change_theme_textColor");
  pCollection.forEach((p) => p.classList.add("change_theme_textColor"));
  aCollection.forEach((a) => a.classList.add("change_theme_textColor"));
}

function removeTheme() {
  body.classList.remove("change_theme_background1");
  footer.classList.remove("change_theme_background1");
  main.classList.remove("change_theme_background2");
  h1.classList.remove("change_theme_textColor");
  h2.classList.remove("change_theme_textColor");
  h3.classList.remove("change_theme_textColor");
  pCollection.forEach((p) => p.classList.remove("change_theme_textColor"));
  aCollection.forEach((a) => a.classList.remove("change_theme_textColor"));
}

// Перевірка значення в localStorage при завантаженні сторінки
window.addEventListener("DOMContentLoaded", function () {
  if (localStorage.getItem("myVariable")) {
    themeNummber = parseInt(localStorage.getItem("myVariable"));

    if (themeNummber === 1) {
      applyTheme();
    }
  }
});
